// Remova o erro
const priceNumber = n => +n.replace('R$', '').replace(',', '.');
const x = priceNumber('R$ 99,99');
console.log(x);


// Crie uma IIFE e isole o escopo
// de qualquer código JS.
(function() {
    console.log('oi');
})();
// Como podemos utilizar
// a função abaixo.
const active = callback => callback();

const somar = function() {
    console.log('teste active');
    
}
active(somar);