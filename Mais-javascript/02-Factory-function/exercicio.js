function $$(selectedElements) {
    const elements = document.querySelectorAll(selectedElements);

    function hide() {
        elements.forEach(element => {
            element.style.display = 'none';
        });
        return $$(selectedElements);
    }
    function show() {
        elements.forEach(element => {
            element.style.display = 'initial';
        });
        return $$(selectedElements);
    }
    return {
        elements,
        hide,
        show
    }
}

const btns = $$('button');

console.log(btns.hide().show());
