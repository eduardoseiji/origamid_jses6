function createButton(text) {
    const numeroSecreto = '6464897487';

    function element() {
        const buttonElement = document.createElement('button');
        buttonElement.innerText = text;
        return buttonElement;
    }
    return {
        text,
        element
    }
}

const btnComprar    = createButton('Comprar');
const btnVender     = createButton('Vender');



function Pessoa(nome) {
    if (!(this instanceof Pessoa)) // ou (!new.target)
      return new Pessoa(nome);
    this.nome = nome;
  }
  
  Pessoa.prototype.andar = function() {
    return `${this.nome} andou`;
  }
  
  const designer = Pessoa('André');
  