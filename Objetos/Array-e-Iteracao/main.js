// const carros = ['Ford', 'Fiat', 'Honda'];

// carros.forEach((item, index, array) => {
//     console.log(item.toUpperCase());
// });

// const li = document.querySelectorAll('li');

// li.forEach(i => i.classList.add('ativa'));

// li.forEach(function(item) {
//   item.classList.add('ativa');
// });

// const carros = ['Ford', 'Fiat', 'Honda'];

// const novaArray = carros.map((item, index, array) => {
//     return item.toUpperCase();
// });

// console.log(carros);
// console.log(novaArray);

// const numeros = [2, 4, 5, 6, 43];
// const numerosx2 = numeros.map(n => n * 2);
// console.log(numerosx2);

// const aulas = [
//     {
//       nome: 'HTML 1',
//       min: 15
//     },
//     {
//       nome: 'HTML 2',
//       min: 10
//     },
//     {
//       nome: 'CSS 1',
//       min: 20
//     },
//     {
//       nome: 'JS 1',
//       min: 25
//     },
//   ]
  
//   const tempoAulas = aulas.map(aula => aula.min);
//   // [15, 10, 20, 25];
  
//   const puxarNomes = aula => aula.nome;
//   const nomesAulas = aulas.map(puxarNomes);
//   // ['HTML 1', 'HTML 2', 'CSS 1', 'JS 1']


// const aulas = [10, 25, 30];

// const reduceAulas = aulas.reduce((acumulador, item, index, array) => {
//     console.log(acumulador, item)
//     return acumulador + item;
// }, 0);

// const numeros = [10, 25, 30, 3, 54, 33, 22];

// const maiorNumero = numeros.reduce((anterior, atual) =>
//     anterior > atual ? anterior : atual, 0);

// console.log(maiorNumero)

// const aulas = [
//     {
//       nome: 'HTML 1',
//       min: 15
//     },
//     {
//       nome: 'HTML 2',
//       min: 10
//     },
//     {
//       nome: 'CSS 1',
//       min: 20
//     },
//     {
//       nome: 'JS 1',
//       min: 25
//     },
//   ]
  
//   const tempoAulas = aulas.map(aula => aula.min);
//   // [15, 10, 20, 25];
  
//   const puxarNomes = aula => aula.nome;
//   const nomesAulas = aulas.map(puxarNomes);
//   // ['HTML 1', 'HTML 2', 'CSS 1', 'JS 1']


//   const frutas = ['Banana', 'Pêra', 'Uva'];

// const frutasRight = frutas.reduceRight((acc, fruta) => acc + ' ' + fruta);
// const frutasLeft = frutas.reduce((acc, fruta) => acc + ' ' + fruta);

// frutasRight; // Uva Pêra Banana
// frutasLeft; // Banana Pêra Uva


// const frutas = ['Banana', 'Pêra', 'Uva'];
// const temUva = frutas.some((item) => {
//     console.log(item);
//     return item === 'Uva';
// });

// {
//     const frutas = ['Banana', 'Pêra', 'Uva', ''];
//     // False pois pelo menos uma fruta
//     // está vazia '', o que é um valor falsy
//     const arraysCheias = frutas.every((fruta) => {
//     return fruta; // false
//     });

//     const numeros = [6, 43, 22, 88, 101, 29];
//     const maiorQue3 = numeros.every(x => x > 3); // true


// }


// const frutas = ['Banana', 'Pêra', 'Uva', 'Maçã'];
// const buscaUva = frutas.findIndex((fruta) => {
//   return fruta === 'Uva'; 
// }); // 2

// console.log(buscaUva);

// const numeros = [6, 43, 22, 88, 101, 29];
// const buscaMaior45 = numeros.find(x => x > 45); // 88

const frutas1 = ['Banana', undefined, null, '', 'Uva', 0, 'Maçã'];

const arrayFrutas = frutas1.filter((item) => {
    console.log(item);
    return item;
});

console.log(arrayFrutas);