// const instrumentos = ['Guitarra', 'Baixo', 'Violão'];
// const precos = [49, 99, 69, 89];

// const dados = [new String('Tipo 1'), ['Carro', 'Portas', {cor: 'Azul', preco: 2000}], function andar(nome) { console.log(nome) }];

// const carros = new Array('Ford', 'Fiat', 'Honda');

// console.log(carros);

// carros[2] = 'Ferrari';
// carros[20] = 'Mustang'; //deixa espaços vazios

// const li = document.querySelectorAll('li');

// const obj = {
//     0: 'Eduardo',
//     1: 'Seiji',
//     2: 'Teste',
//     length: 3
// }

// const arrayObj = Array.from(obj);

// console.log(li);
// const arrayLi = Array.from(li);

// console.log(arrayLi);

// console.log(instrumentos);

// const idades = [32,21,33,43,1,12,8];
// idades.sort();
// console.log(idades);

const carros = ['Ford', 'Fiat', 'VW'];
carros.unshift('Kia', 'Ferrari'); //adiciona no começo mas o retorno disso é o tamanho atual da array
carros.push('Parati', 'Gol'); //adiciona no final mas o retorno disso é o tamanho atual da array


// console.log(carros);
// carros.reverse();

// console.log(carros);
// carros.splice(1, 0, 'Fusca');
// console.log(carros);

console.log(['Item1','Item2','Item3','Item4', 'Item5'].copyWithin(2, 0, 2));

console.log(['Item1','Item2','Item3','Item4', 'Item5'].fill('Banana', 0, 2));

const transporte1 = ['Barco', 'Aviao'];
const transporte2 = ['Carro', 'Moto'];
const transportes = transporte1.concat(transporte2);
// ['Barco', 'Aviao', 'Carro', 'Moto'];

const maisTransportes = [].concat(transporte1, transporte2, 'Van');
// ['Barco', 'Aviao', 'Carro', 'Moto', 'Van'];
// console.log(maisTransportes)

// const linguagens = ['html', 'css', 'js', 'php', 'python', 'js'];

// linguagens.includes('css'); // true
// linguagens.includes('ruby'); // false
// linguagens.indexOf('python'); // 4
// linguagens.indexOf('js'); // 2
// linguagens.lastIndexOf('js'); // 5

// let htmlString = '<h2>Título Principal</h2>';
// htmlString = htmlString.split('h2');
// htmlString = htmlString.join('');

// console.log(htmlString);

const linguagens = ['html', 'css', 'js', 'php', 'python'];

console.log(linguagens.slice(2,4));
