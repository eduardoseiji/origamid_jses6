{    
    const comidas = ['Pizza', 'Frango', 'Carne', 'Macarrão'];
    // Remova o primeiro valor de comidas e coloque em uma variável
    // Remova o último valor de comidas e coloque em uma variável
    // Adicione 'Arroz' ao final da array
    // Adicione 'Peixe' e 'Batata' ao início da array
    let primeiroValor = comidas.shift();
    let ultimoValor = comidas.pop();
    comidas.unshift('Peixe', 'Batata')
    comidas.push('Arroz')
    console.log(primeiroValor);
    console.log(ultimoValor);

}

{
    const estudantes = ['Marcio', 'Brenda', 'Joana', 'Kleber', 'Julia'];
    // Arrume os estudantes em ordem alfabética
    // Inverta a ordem dos estudantes
    // Verifique se Joana faz parte dos estudantes
    // Verifique se Juliana faz parte dos estudantes
    estudantes.sort();
    console.log(estudantes)
    estudantes.reverse();
    console.log(estudantes)
    console.log(estudantes.includes('Joana'));
    console.log(estudantes.includes('Juliana'));
}

{
    let html = `<section>
              <div>Sobre</div>
              <div>Produtos</div>
              <div>Contato</div>
            </section>`
    // Substitua section por ul e div com li,
    // utilizando split e join
    let htmlArray = html.split('section');
    htmlArray = htmlArray.join('ul');
    htmlArray = htmlArray.split('div');
    htmlArray = htmlArray.join('li');    
    console.log(htmlArray);
    // let htmlArray = html.split('section').join('ul').split('div').join('li');
}

const carros = ['Ford', 'Fiat', 'VW', 'Honda'];
// Remova o último carro, mas antes de remover
// salve a array original em outra variável
let carrosNovoArray = carros.slice(0);
carros.pop();
console.log(carros);

