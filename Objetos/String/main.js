// const comida = 'Pizza';
// const agua = new String('Agua');

// console.log(comida.length);
// console.log(agua.length);

// const frase = 'A melhor comida';

// console.log(frase[frase.lenght]) //undefined
// console.log(frase.charAt(frase.lenght)); //retorna nada

const frase = 'A melhor linguagem é:';
const linguagem = 'Javascript';

const fraseFinal = frase.concat(linguagem, '!!', 'Oi sumido');

let frutas = 'Banana';
let listaFrutas = 'Melancia, Banana, Laranja';

console.log(listaFrutas.includes(frutas, 10));

console.log(frutas.startsWith('Ba')); //true
console.log(frutas.startsWith('ba')); //false
console.log(frutas.endsWith('na')); //true

const transacao1 = 'Deposito do cliente';
const transacao2 = 'Deposito do fornecedor';
const transacao3 = 'Taxa de camisas';

console.log(transacao1.slice(0, -2));

let preco = 'R$ 99,90';

console.log(preco.padEnd(10, 'abcdefghijk'));

const fraseRepeat = 'Ta';

console.log(fraseRepeat.repeat(5));

let listaItens = 'Camisas Bonés Calças Bermudas Vestidos Saias';
listaItens = listaItens.replace(/[ ]+/g, ', ');

const arrayLista = listaItens.split('');

console.log(arrayLista);

const htmlText = '<div>O melhor item</div><div>A melhor lista</div>';
const htmlArray = htmlText.split('div');
const novoHtml = htmlArray.join('section');

console.log(htmlText);
console.log(htmlArray);

const frutasArray = ['Banana', 'Melancia', 'Laranja'];

const sexo1 = 'Feminino';
const sexo2 = 'feminino';
const sexo3 = 'FEMININO';

(sexo1.toLowerCase() === 'feminino'); // true
(sexo2.toLowerCase() === 'feminino'); // true
(sexo3.toLowerCase() === 'feminino'); // true

const valor = '  R$ 23.00   ' 
valor.trim(); // 'R$ 23.00'
valor.trimStart(); // 'R$ 23.00   '
valor.trimEnd(); // '  R$ 23.00'
