// const perimetro = new Function('lado', 'return lado * 4');

// function somar(n1, n2) {
//     return n1 + n2 + ' 2';
// }
  
// somar.length; // 2
// somar.name; // 'somar'
// console.log(somar(3,3))

// function darOi() {
//     console.log('Eae man');
// }

// darOi.call();

// function descricaoCarro() {
//     console.log(this.marca + ' ' + this.ano);
// }

// descricaoCarro.call({marca: 'Honda', ano: 2015});

// const carros = ['Ford', 'Fiat', 'VW'];
// const frutas = ['Banana', 'Uva', 'Pera'];

// carros.forEach.call(frutas, (item) => {
//     console.log(item);
// });

// function Dom(seletor) {
//     this.element = document.querySelector(seletor);
// }

// Dom.prototype.ativo = function(classe) {
//     console.log(this)
//     this.element.classList.add(classe);
// }

// const ul = new Dom('ul');

// const li = {
//     element: document.querySelector('li')
// }

// Dom.prototype.ativo();

// ul.ativo.call(li, 'ativar');
// ul.ativo('ativar');

const frutas = ['Uva', 'Maça', 'Banana'];

Array.prototype.pop.call(frutas);
frutas.pop();

const arrayLike = {
    0: 'Item 1',
    1: 'Item 2',
    2: 'Item 3',
    length: 3
}

const li = document.querySelectorAll('li');
const arrayLi = Array.from(li);

const filtro = arrayLi.filter((item) => {
    return item.classList.contains('ativo');
});

console.log(filtro)

console.log(li)

const numeros = [33,4232,4232,212,45,423,1,56,43];
console.log(Math.max.apply(null, numeros))

const $ = document.querySelectorAll.bind(document);

const carro = {
    marca: 'Ford',
    ano: 2018,
    acelerar: function(aceleracao, tempo) {
      return `${this.marca} acelerou ${aceleracao} em ${tempo}`;
    }
  }
  carro.acelerar(100, 20);
  // Ford acelerou 100 em 20
  
  const honda = {
    marca: 'Honda',
  };
  const acelerarHonda = carro.acelerar.bind(honda);
  acelerarHonda(200, 10);
  // Honda acelerou 200 em 10
  