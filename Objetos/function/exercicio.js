{
    // Retorne a soma total de caracteres dos
    // parágrafos acima utilizando reduce
    const p = document.querySelectorAll('p');
    let arrayP = Array.from(p);
    const totalChar = arrayP.reduce((acumulador, item) => {
        return acumulador + item.innerText.length;
    }, 0);

    console.log(totalChar);
}

{
    // Crie uma função que retorne novos elementos
    // html, com os seguintes parâmetros
    // tag, classe e conteudo.
    function novoElemento(tag, classe, conteudo) {
        const elemento = document.createElement(tag);
        classe ? elemento.classList.add(classe) : null;
        conteudo ? elemento.innerHTML = conteudo : null;
        return elemento;
    }
    console.log(novoElemento('h1', 'ativo', 'Lorem'));

    let novoElementoh1 = novoElemento.bind(null, 'h1', 'ativo');

    console.log(novoElementoh1('Loremzim'));

    
}