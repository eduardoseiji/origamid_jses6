function Carro(marca, preco) {
    this.marca = marca;
    this.preco = preco;
}

const honda = new Carro('Honda', 4000);
const fiat  = new Carro('Fiat', 3000);

function Carro2(marca, precoInicial) {
    this.taxa = 1.2;
    const precoFinal = precoInicial * this.taxa;
    this.marca = marca;
    this.preco = precoFinal;
    console.log(this);
}

const mazda = new Carro2('Mazda', 5000);

// const Dom = {
//     seletor: 'li',
//     element() {
//         return document.querySelector(this.seletor);
//     },
//     ativar() {
//         this.element.classList.add('ativar');
//     }
// }

function Dom(seletor) {
    this.element = function() {
        return document.querySelector(seletor);
    },
    this.ativar = function() {
        this.element().classList.add('ativar');
    }
}

const li = new Dom('li');
const ul = new Dom('ul');

const lastLi = new Dom('li:last-child');

lastLi.ativar();