// function Pessoa(nome, idade) {
//     this.nome = nome;
//     this.idade = idade;
// }

// var obj = {
//     nome: 'eduardo',
//     idade: 22
// }

// obj.teste = 'Isso';

// Pessoa.prototype.andar = function() {
//     return 'Pessoa andou';
// }

// const eduardo = new Pessoa('eduardo', 22);

// console.log(Pessoa.prototype);
// console.log(eduardo.prototype);

const pais = 'Brasil';

const cidade = new String('Rio');


const listaAnimais = ['Cachorro', 'Gato', 'Cavalo'];

const lista = document.querySelectorAll('li');

const listaArray = Array.prototype.slice.call(lista);

const listaArray2 = Array.from(lista);

const Carro = {
    marca: 'Ford',
    preco: 3000,
    andar(){
        return true;
    }
}