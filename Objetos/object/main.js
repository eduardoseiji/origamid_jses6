// const pessoa = new Object({
//     nome: 'Eduardo'
// });

// const carro = {
//     rodas: 4,
//     init(valor){
//         this.marca = valor;
//         return this;
//     },
//     acelerar() {
//         return this.marca + ' acelerou';
//     },
//     buzinar() {
//         return this.marca + 'Buzinou';
//     }
// }

// const honda = Object.create(carro).init('Honda');
// const ferrari = Object.create(carro).init('Ferrari');

// const funcaoAutomovel = {
//     acelerar() {
//         return 'acelerou';
//     },
//     buzinar() {
//         return 'buzinou';
//     },
// }

// const moto = {
//     rodas: 2,
//     capacete: true
// }
// Object.assign(moto, funcaoAutomovel, carro);

// console.log(moto)

// console.log(moto)
// moto.rodas = 5
// console.log(moto)

const moto = {
    capacete: true
}


Object.defineProperties(moto, {
    rodas: {
        get() {
            return this._rodas;
        },
        set(valor) {
            this._rodas = valor * 4 + ' Total Rodas'
        }
    }
})

console.log(moto)

Object.getOwnPropertyDescriptors(Array);
// Lista com métodos e propriedades e Array

Object.getOwnPropertyDescriptors(Array.prototype);
// Lista com métodos e propriedades do protótipo de Array

Object.getOwnPropertyDescriptor(window, 'innerHeight');
// Puxa de uma única propriedade

Object.keys(Array);
// [] vazia, pois não possui propriedades enumeráveis

const carro = {
  marca: 'Ford',
  ano: 2018,
}
Object.keys(carro);
// ['marca', 'ano']
Object.values(carro);
// ['Ford', 2018]
Object.entries(carro);
// [['marca', 'Ford'], ['ano', 2018]]

const frutas = ['Banana', 'Pêra']
Object.getPrototypeOf(frutas);
Object.getPrototypeOf(''); // String

const frutas1 = ['Banana', 'Pêra'];
const frutas2 = ['Banana', 'Pêra'];

Object.is(frutas1, frutas2); // false

// Object.freeze(carro);
// Object.seal(carro);
// Object.preventExtensions(carro);
carro.marca = 'Honda';
delete carro.marca;
console.log(carro)


const frutas = ['Banana', 'Uva'];
frutas.toString(); // 'Banana,Uva'
typeof frutas; // object
Object.prototype.toString.call(frutas); // [object Array]

const frase = 'Uma String';
frase.toString(); // 'Uma String'
typeof frase; // string
Object.prototype.toString.call(frase); // [object String]

const carro = {marca: 'Ford'};
carro.toString(); // [object Object]
typeof carro; // object
Object.prototype.toString.call(carro); // [object Object]

const li = document.querySelectorAll('li');
typeof li; // object
Object.prototype.toString.call(li); // [object NodeList]