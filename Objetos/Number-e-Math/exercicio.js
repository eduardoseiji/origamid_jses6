// Retorne um número aleatório
// entre 1050 e 2000

{
    let numeroMinimo = 1050;
    let numeroMaximo = 2000;

    let numeroFinal = Math.floor(Math.random() * (numeroMaximo - numeroMinimo + 1) + numeroMinimo);

    console.log(numeroFinal);
}


{
    // Retorne o maior número da lista abaixo
    const numeros = '4, 5, 20, 8, 9, 45, 50';

    let numerosArray = numeros.split(', ');
    // let numeroMaximo = Math.max(...numerosArray);
    // console.log(numeroMaximo)
    let maiorNumero = 0;
    numerosArray.forEach((item) => {
        if(maiorNumero < +item) {
            maiorNumero = item;
        }
    });
    console.log(maiorNumero);

}

{
    // Crie uma função para limpar os preços
    // e retornar os números com centavos arredondados
    // depois retorne a soma total
    const listaPrecos = ['R$ 59,99', ' R$ 100,222',
                        'R$ 230  ', 'r$  200'];
    let itemFinal;    
    let soma = 0;
    listaPrecos.forEach((item, index) => {
        itemFinal = +item.toUpperCase().replace('R$', '').trim().replace(',', '.');
        itemFinal = +itemFinal.toFixed(2);
        soma += itemFinal;
    });
    console.log(soma);
}