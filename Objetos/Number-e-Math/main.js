console.log(Number.isNaN('ds'));
console.log(Number.isInteger(10.4343434));

console.log(parseFloat(' 323312.312')); //323312.312
console.log(parseFloat('100.27 reais')); //100.27
console.log(parseInt('100.27 reais', 10)); //100
console.log(Number.parseInt(23.49, 10)); // 23

const preco = 10.12121;
console.log(+preco.toFixed() + 3232);

let valor = 48.49;
valor = valor.toLocaleString('en-US', {style: 'currency', currency: 'USD'})

Math.abs(-5.5); // 5.5
Math.ceil(4.8334); // 5
Math.ceil(4.3); // 5
Math.floor(4.8334); // 4
Math.floor(4.3); // 4
Math.round(4.8334); // 5
Math.round(4.3); // 4

Math.max(5,3,10,42,2); // 42
Math.min(5,3,10,42,2); // 2


let numeroMinimo = 1;
let numeroMaximo = 6;

const aleatorio = Math.floor(Math.random() * (numeroMaximo - numeroMinimo + 1) + numeroMinimo);
console.log(aleatorio);