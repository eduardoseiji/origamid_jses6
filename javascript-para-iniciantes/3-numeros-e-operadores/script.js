var idade = 22;

var pi = 3.14;

//Tem precisao até 15 digitos, depois começa a arredondar

console.log(idade);

var soma = 100 + 50; // 150
var subtracao = 100 - 50; // 50
var multiplicacao = 100 * 2; // 200
var divisao = 100 / 2; // 50
var expoente = 2 ** 4; // 16
var modulo = 14 % 5; // 4

//Sempre que voce somar uma string, voce concatena ela

var somaString = '100' + 50; //10050
var subtracaoString = '100' - 50; //Nesse caso, como nao existe subtracao de string, ele tenta converter
var  multiplicacaoString = '100' * '2'; //200
var divisaoString = 'Comprei 10' / 2; //Not a number
console.log(divisaoString);
console.log(isNaN(divisaoString));

var total1 = 20 + 5 * 2; // 30
var total2 = (20 + 5) * 2; // 50
var total3 = 20 / 2 * 5; // 50
var total4 = 10 + 10 * 2 + 20 / 2; // 40



var soma2 = 10 + 10 + 20 + 30 * 4 / 2 + 10;
console.log(soma2);

var incremento = 1;

console.log(++incremento);

var frase = 'Isso é um teste';
+frase; // NaN
-frase; // NaN

var idade = 22;
-idade; // -22

var num = +'80' / 2;
var unidade = 'kg';
var peso = num + unidade;
console.log(peso);
