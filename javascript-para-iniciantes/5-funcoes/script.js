//Funções

function areaQuadrado(lado) {
    return lado * lado;
}

console.log(areaQuadrado(2));

function pi() {
    return 3.14;
}   

var total = 5 * pi();

console.log(pi);

function imc(peso, altura) {
    const imc = peso / (altura * altura);
    return imc;
}

console.log(imc(120, 1.8));

function corFavorita(cor) {
    if(cor === 'azul') {
        return 'Você gosta do céu!';
    } else if (cor === 'verde') {
        return 'Voce gosta da floresta!';
    } else {
        return 'Você nao gosta de nada';
    }
}

// addEventListener('click', function() {
//     console.log('oi');
// });

function mostraConsole() {
    console.log('oe');
}

addEventListener('click', mostraConsole);

function imc2(peso, altura) {
    const imc = peso / (altura ** 2);
    console.log(imc);
}

imc2(120, 1.82);

function terceiraIdade(idade) {
    if(typeof idade !== 'number') {
        return 'Por favor, preencha um número!';
    } else if(idade >= 60) {
        return true;
    } else {
        return false;
    }
}


console.log(terceiraIdade('aaa'));


function faltaVisitar(paisesVisitados) {
    var totalPaises = 193;
    return `Falta visitar ${totalPaises - paisesVisitados} países`;
}

var profissao = 'Designer';

function dados() {
  var nome = 'André';
  var idade = 28;
  function outrosDados() {
    var endereco = 'Rio de Janeiro';
    var idade = 29;
    return `${nome}, ${idade}, ${endereco}, ${profissao}`;
  }
  return outrosDados();
}

console.log(dados()); // Retorna 'André, 29, Rio de Janeiro, Designer'
// outrosDados(); // retorna um erro
