// Crie uma função para verificar se um valor é Truthy
function verificarTruthy(valor) {
    return !!valor;
}
console.log(verificarTruthy(1));
// Crie uma função matemática que retorne o perímetro de um quadrado
// lembrando: perímetro é a soma dos quatro lados do quadrado
function ladoQuadrado(lado) {
  var perimetro = lado ** 2;
  console.log(`O lado do quadrado é ${perimetro}`);
}

ladoQuadrado(2);

// Crie uma função que retorne o seu nome completo
// ela deve possuir os parâmetros: nome e sobrenome
function nomeCompleto(nome, sobrenome) {
  var nomeCompleto = `${nome} ${sobrenome}`;
  return console.log(nomeCompleto);
}

nomeCompleto('Eduardo Seiji', 'Castro Silva')

// Crie uma função que verifica se um número é par
function isEven(numero) {
  if(numero % 2 === 0) {
    console.log('É par');
  } else {
    console.log('É impar');
  }
}

// Crie uma função que retorne o tipo de
// dado do argumento passado nela (typeof)
function tipoDedado(dado) {
  var tipoDado = typeof dado;
  return tipoDado;
}

// addEventListener é uma função nativa do JavaScript
// o primeiro parâmetro é o evento que ocorre e o segundo o Callback
// utilize essa função para mostrar no console o seu nome completo
// quando o evento 'scroll' ocorrer.
addEventListener('scroll', function(){
  console.log('Scroll!!');
}); 

// Corrija o erro abaixo
function precisoVisitar(paisesVisitados) {
  var totalPaises = 193;
  return `Ainda faltam ${totalPaises - paisesVisitados} países para visitar`;
  function jaVisitei(paisesVisitados) {
    return `Já visitei ${paisesVisitados} do total de ${totalPaises} países`;
  }
}


precisoVisitar(20);
jaVisitei(20);
  