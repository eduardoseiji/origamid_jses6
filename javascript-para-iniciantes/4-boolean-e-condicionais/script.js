var possuiGraduacao = false;
var possuiDoutorado = true;

if(possuiGraduacao) {
    console.log('aa');
    var x = 10;
} else if(possuiDoutorado){
    console.log('Tem doutorado');
} else {
    console.log('foda..');
}

console.log(x);

var nome = 'Eduardo';

if(nome) {
    console.log(nome);
} else {
    console.log('Nome inexistente');
}

// true && true; //true
// true && false; //false
// false && true; //false
// 'Gato' && 'Cao'; //Cao
// (5-5) && (5+5); // 0, Por que 0 é um valor false
// 'Gato' && false; //false
// (5 >= 5) && (3 < 6); //true

if((5-5) && (5+5)) {
    console.log('1 - Verdadeiro');
} else {
    console.log('1 -Falso');
}

var condicional = (5 - 10) && (5 + 5);
if(condicional) {
    console.log('2 - Verdadeiro');
} else {
    console.log('2 - Falso');
}


// true || true; // true
// true || false; // true
// false || true; // true
// 'Gato' || 'Cão'; // 'Gato'
// (5 - 5) || (5 + 5); // 10
// 'Gato' || false; // Gato
// (5 >= 5) || (3 < 6); // true


var condicional2 = (5 - 5) || (5 + 5) || (10 - 2);
console.log(condicional);

var corFavorita = 'Roxo';

switch(corFavorita) {
    case 'Azul' :
        console.log('Olhe para o céu');
        break;
    case 'Verde':
        console.log('Olhe para a floresta');
        break;
    case 'Amarelo':
        console.log('Olhe para o sol');
        break;
    default:
        console.log('Feche os olhos');
}