var nome = 'Eduardo'; //string
var idade = 28; //Número
var possuiFaculdade = true; //Boolean
var time; //undefined
var comida = null;
var simbolo = Symbol(); //Symbom
var novoObjeto = {} //Object


console.log(typeof idade, typeof nome);

var infos = nome + ' idade: ' + idade;

console.log(infos);

var mes = 1;

console.log(typeof mes);

var aspas = "É possivel começar com aspas 'e ter apostrofo no meio' e vice-versa";
console.log(aspas);

var backslash = 'Da pra usar \'backslash no meio\' tambem';
console.log(backslash);

var gols = 1000;
var frase = 'Pelé fez' + gols + 'gols';

var frase = `Pelé fez ${gols * 2} gols, e nem todos foram bonitos`;
console.log(frase);