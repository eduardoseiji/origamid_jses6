'use strict' //Nao deixa usar variaveis globais e "muda" o funcionamento do eval()


function mostrarCarro() {
    carro = 'Fusca';
    console.log(carro);
  }
  
  mostrarCarro(); // Fusca
  console.log(carro); // Fusca


if(true) {
    var carro2 = 'Fusca';
    console.log(carro2);
}

console.log(carro2);

{
    var carro = 'Fusca';
    let ano = 2018;
}
  console.log(carro); // Carro
  console.log(ano); // erro ano is not defined

let i = 50;

for(let i = 0; i < 10; i++) {
    console.log(`Número ${i}`);
}
console.log(i * 10); // 500

const dia = 'Sexta';
      dia = 'Quinta';

console.log(dia);