// nomeie 3 propriedades ou métodos de strings
var nome = 'Eduardo';
/* .charAt() Se nao passar parametro, passa o primeiro caracter */
/* .bold() Envolve o conteudo em uma tag <b>
The indexOf() method returns the position of the first occurrence of a specified value in a string.
This method returns -1 if the value to search for never occurs.
Note: The indexOf() method is case sensitive. */


// nomeie 5 propriedades ou métodos de elementos do DOM
var btn = document.querySelector('body');
// .addEventListener(x, function(){});
// .classList.add('foo')
// .style.display = "block"
// .lang = "pt-br"

// busque na web um objeto (método) capaz de interagir com o clipboard, 
// clipboard é a parte do seu computador que lida com o CTRL + C e CTRL + V

//clipboard.js
