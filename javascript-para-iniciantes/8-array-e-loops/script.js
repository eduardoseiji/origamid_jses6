var videoGames = ['Switch', 'PS4', 'Xbox', '3DS'];

// var ultimoItem = videoGames.pop();
videoGames.push('PC');
console.log(videoGames);

for (var numero = 0; numero < 10; numero++) {
    console.log(numero);
}

console.log('While:')
var i = 0;
while(i <= 10) {
    console.log(i);
    i++;
}


for(var item = 0; item < videoGames.length; item++) {
    console.log(videoGames[item]);
    if(videoGames[item] == 'PS4'){
        break;
    }
}

var frutas = ['Banana', 'Pera', 'Maçã', 'Abacaxi', 'Uva']
videoGames.forEach(function(fruta, index, lista){
    console.log(fruta, index, lista);
});

var numero = 0;
var maximo = 50;
for(;numero < maximo;) {
    console.log(numero);
    numero++;
}