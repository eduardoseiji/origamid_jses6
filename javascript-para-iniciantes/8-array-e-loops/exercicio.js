// Crie uma array com os anos que o Brasil ganhou a copa
// 1959, 1962, 1970, 1994, 2002
var copasDoBrasil = [1959, 1962, 1970, 1994, 2002];

// Interaja com a array utilizando um loop, para mostrar
// no console a seguinte mensagem, `O brasil ganhou a copa de ${ano}`
copasDoBrasil.forEach(function(ano){
    console.log(`O Brasil ganhou a copa no ano de ${ano}`)
});

// Interaja com um loop nas frutas abaixo e pare ao chegar em Pera
var frutas = ['Banana', 'Maçã', 'Pera', 'Uva', 'Melância'];

for(i = 0; i <= frutas.length; i++){
    console.log(frutas[i]);
    if(frutas[i] === 'Uva') {
        break;
    }
}

// Coloque a última fruta da array acima em uma variável,
// sem remover a mesma da array.
console.log('Tirar o ultimo da array');
var pecas = ['cpu', 'vga', 'gabinete', 'mouse', 'teclado'],
    ultimoItem = pecas[pecas.length - 1];
    
    console.table(ultimoItem, pecas);