import outsideClick from "./outsideclick.js";

export default function initMenuMobile() {
    let btnMenu     = document.querySelector('[data-menu="button"]');
    let menuMobile  = document.querySelector('[data-menu="list"]');
    let eventos  = ['click', 'touchstart'];

    if(btnMenu) {
        function toggleMenu() {
            btnMenu.classList.add('menu-ativo');
            menuMobile.classList.add('menu-ativo');
            outsideClick(menuMobile, eventos, () => {
                btnMenu.classList.remove('menu-ativo');
                menuMobile.classList.remove('menu-ativo');
            });
        }
        eventos.forEach(evento => btnMenu.addEventListener(evento, toggleMenu))
    }
}
