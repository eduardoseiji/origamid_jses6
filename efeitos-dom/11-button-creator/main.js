const controles     = document.querySelector('#controles');
const btn           = document.querySelector('.btn');
const cssText       = document.querySelector('.css');

controles.addEventListener('change', handleChange);

const handleStyle = {
    element: btn,
    backgroundColor(value) {
        this.element.style.backgroundColor = value;
    },
    height(value) {
        this.element.style.height = value + 'px';
    },
    width(value) {
        this.element.style.width = value + 'px';
    },
    texto(value) {
        this.element.innerText = value;
    },
    border(value) {
        this.element.style.border = value;
    },
    borderRadius(value) {
        this.element.style.borderRadius = value + '%';
    },
    fontFamily(value) {
        this.element.style.fontFamily = value;
    },
    fontSize(value) {
        this.element.style.fontSize = value + 'px';
    },
    color(value) {
        this.element.style.color = value;
    }

}

function handleChange(e) {
    const name  = e.target.name;
    const value = e.target.value;
    handleStyle[name](value);

    getValues(name, value);
    showCss();
}

function showCss() {
    cssText.innerHTML = '<span>' + btn.style.cssText.split('; ').join(';</span><span>');
    
}

function getValues(name, value) {
    localStorage[name] = value;
}

console.log(controles.element);
function setValues() {
    const properties = Object.keys(localStorage);
    properties.forEach((item) => {
        controles.elements[item].value = localStorage[item];
        handleStyle[item](localStorage[item]);
        showCss();
    });
}

setValues();