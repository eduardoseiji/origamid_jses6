export default function initTooltip() {

}

const tooltip = document.querySelectorAll('[data-tooltip]');

tooltip.forEach((item) => {
    item.addEventListener('mouseover',onMouseOver);
});

function onMouseOver(e) {
    const tooltipBox = criarTooltipBox(this);
    tooltipBox.style.top = e.pageY + 'px';
    tooltipBox.style.left = e.pageX + 'px';
    
    onMouseMove.tooltipBox = tooltipBox;
    this.addEventListener('mousemove', onMouseMove);
    
    
    onMouseLeave.element = this;
    onMouseLeave.tooltipBox = tooltipBox;
    this.addEventListener('mouseleave', onMouseLeave);

}

const onMouseLeave = {
    tooltipBox: '',
    handleEvent() {
        this.tooltipBox.remove();
        this.element.removeEventListener('mouseleave', onMouseLeave);
        this.element.removeEventListener('mousemove', onMouseMove);
    }
}

const onMouseMove = {
    handleEvent(e) {
        this.tooltipBox.style.top = e.pageY + 20 +  'px';
        this.tooltipBox.style.left = e.pageX + 20 +'px';
    }
}

function criarTooltipBox(element) {
    const tooltipBox = document.createElement('div');
    const text = element.getAttribute('aria-label')
    tooltipBox.classList.add('tooltip');
    tooltipBox.innerText = text;
    document.body.appendChild(tooltipBox);
    return tooltipBox;
}