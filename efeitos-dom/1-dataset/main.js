function initTabNav() {
    let tabMenu = document.querySelectorAll('[data-tab="menu"] li');
    let tabContent = document.querySelectorAll('[data-tab="content"] section');    
    let arrayTabContent = Array.from(tabContent);
    arrayTabContent[0].classList.add('ativo', arrayTabContent[0].dataset.anime);
    if(tabMenu.length && tabContent.length) {
        function activeTab(index) {
            tabContent.forEach((section) => {
                section.classList.remove('ativo');
            });
            let direcao = tabContent[index].dataset.anime;
            tabContent[index].classList.add('ativo', direcao);
        }

        tabMenu.forEach((itemMenu, index) => {
            itemMenu.addEventListener('click', () => {
                activeTab(index);
            });
        });
    }
}

initTabNav();


function initAccordion() {
    const accordionList = document.querySelectorAll('[data-anime="accordion"] dt');
    let activeClass = 'ativo';
    if(accordionList.length) {
        accordionList[0].classList.add(activeClass);
        accordionList[0].nextElementSibling.classList.add(activeClass);
        function activeAccordion() {
            this.classList.toggle(activeClass);
            this.nextElementSibling.classList.toggle(activeClass);
        }

        accordionList.forEach((item) => {
            item.addEventListener('click', activeAccordion);
        });
    }
}
initAccordion();

function initScrollSuave() {
    const linksInternos = document.querySelectorAll('[data-menu="suave"] a[href^="#"]');

    linksInternos.forEach((link) => {
        link.addEventListener('click', scrollToSection);
    });

    function scrollToSection(event) {
        event.preventDefault();
        const href = event.currentTarget.getAttribute('href');
        const section = document.querySelector(href);
        
        section.scrollIntoView({
            behavior: 'smooth',
            block: 'start',
        });

        // forma alternativa
        // const top = section.offsetTop;
        // window.scrollTo({
        //     top: topo,
        //     behavior: 'smooth',
        // });
    }
}
initScrollSuave();

const sections = document.querySelectorAll('[data-anime="scroll"]');
if(sections.length) {
    const windowMetade = window.innerHeight * 0.6;

    function animaScroll() {
        sections.forEach((section) => {
            const sectionTop = section.getBoundingClientRect().top; 
            const isSectionVisible = (sectionTop - windowMetade) < 0;
            windowMetade;
            if(isSectionVisible) {
                section.classList.add('ativo');
            } else {
                section.classList.remove('ativo');
            }
        });
    }
    window.addEventListener('scroll', animaScroll);
    animaScroll();
}