import initScrollSuave  from './modules/scroll-suave.js';
import initAccordion    from './modules/accordion.js';
import initAnimaScroll  from './modules/scroll-animacao.js';
import initTabNav       from './modules/tab-nav.js';
import initModal        from './modules/modal.js';

initScrollSuave();
initAccordion();
initAnimaScroll();
initTabNav();
initModal();

let data = new Date();
console.log(data);
