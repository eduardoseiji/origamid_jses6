// Mude a cor da tela para azul e depois para vermelho a cada 2s.
// {
//     let corpo = document.querySelector('body');
//     function mudarClasse(el) {
//         el.classList.toggle('ativo');
//     }

//     setInterval(mudarClasse, 2000, corpo);
// }

// Crie um cronometro utilizando o setInterval. Deve ser possível
// iniciar, pausar e resetar (duplo clique no pausar).

{
    let iniciar = document.querySelector('[data-start]');
    let pause = document.querySelector('[data-pause]');
    let reset = document.querySelector('[data-reset]');
    let tempo = document.querySelector('[data-tempo]');

    let i = 0;
    let timer;

    iniciar.addEventListener('click', iniciaContador);
    pause.addEventListener('click', pausaContador);
    reset.addEventListener('click', resetaContador);

    function iniciaContador() {
        timer = setInterval(() => {
            tempo.innerText = i++;
        }, 1000);
        iniciar.setAttribute('disabled', '');
    }
    function pausaContador() {
        clearInterval(timer);
        iniciar.removeAttribute('disabled');
    }
    function resetaContador() {
        i = 0;
        clearInterval(timer);
        tempo.innerText = 0;
        iniciaContador();
        iniciar.removeAttribute('disabled');
    }
}