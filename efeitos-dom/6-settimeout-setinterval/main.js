// function espera(texto) {
//     console.log(texto);
// }
// setTimeout(espera, 1000, 'Passou 1s');
// const btn = document.querySelector('button');
// btn.addEventListener('click', handleClick);

// function handleClick() {
//     setTimeout(() => {
//         console.log(this);
//     }, 1000);
// }
// for (let i = 0; i < 20; i++) {
//     setTimeout(function() {
//         console.log(i);    
//     }, 1000 * i);
// }

function loop(texto) {
    console.log(texto);
}

let i = 0;
const meuLoop = setInterval(() => {
    console.log(i++);
    if(i > 20) {
        clearInterval(meuLoop);
    }
}, 300);