export default function initTabNav() {
    let tabMenu = document.querySelectorAll('[data-tab="menu"] li');
    let tabContent = document.querySelectorAll('[data-tab="content"] section');    
    let arrayTabContent = Array.from(tabContent);
    arrayTabContent[0].classList.add('ativo', arrayTabContent[0].dataset.anime);
    if(tabMenu.length && tabContent.length) {
        function activeTab(index) {
            tabContent.forEach((section) => {
                section.classList.remove('ativo');
            });
            let direcao = tabContent[index].dataset.anime;
            tabContent[index].classList.add('ativo', direcao);
        }

        tabMenu.forEach((itemMenu, index) => {
            itemMenu.addEventListener('click', () => {
                activeTab(index);
            });
        });
    }
}