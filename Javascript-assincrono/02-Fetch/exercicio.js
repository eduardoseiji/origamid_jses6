// Utilizando a API https://viacep.com.br/ws/${CEP}/json/
// crie um formulário onde o usuário pode digitar o cep
// e o endereço completo é retornado ao clicar em buscar
let numCep = 16025320;

let body        = document.querySelector('body');
let form        = document.createElement('form');
let inputCep    = document.createElement('input');
let btnEnviar   = document.createElement('button');
let resultado   = document.createElement('div');
btnEnviar.type = "submit";
btnEnviar.innerText = "submit";

inputCep.name = 'cep';
inputCep.placeholder = 'Digite aqui seu CEP';

form.appendChild(inputCep);
form.appendChild(btnEnviar);

body.appendChild(form);
body.appendChild(resultado);


btnEnviar.addEventListener('click', handleClick);

function handleClick(e) {
    e.preventDefault();
    const cep = inputCep.value;
    buscaCep(cep);
}

function buscaCep(cep) {
    fetch(`https://viacep.com.br/ws/${cep}/json/`)
    .then(r => r.text())
    .then(body => {
        resultado.innerText = body;
    });
}

// Utilizando a API https://blockchain.info/ticker
// retorne no DOM o valor de compra da bitcoin and reais.
// atualize este valor a cada 30s
const sectionBitcoin = document.createElement('section');
sectionBitcoin.classList.add('sectionBitcoin');
body.appendChild(sectionBitcoin);
const labelReal = document.createElement('label');
const labelDolar = document.createElement('label');
labelReal.innerText = 'Real: ';
labelDolar.innerText = 'Dolar: ';
body.appendChild(labelReal);
body.appendChild(labelDolar);

function fetchBTC() {
    fetch('https://blockchain.info/ticker')
    .then(r => r.json())
    .then((r) => {
        labelReal.innerText     = `${r.BRL.symbol} ${r.BRL.buy} \n`;
        labelDolar.innerText    =  `${r.USD.symbol} ${r.USD.buy} \n`;
    });
}
setInterval(fetchBTC, 3000);

// Utilizando a API https://api.chucknorris.io/jokes/random
// retorne uma piada randomica do chucknorris, toda vez que
// clicar em próxima

let divPiada = document.createElement('div');
const imgPiada = document.createElement('img');
const btnPiada = document.createElement('button');

btnPiada.innerText = 'Proxima';

body.appendChild(divPiada);
body.appendChild(imgPiada);
body.appendChild(btnPiada);

btnPiada.addEventListener('click', piadaNorris);

function piadaNorris() {
    fetch('https://api.chucknorris.io/jokes/random')
    .then(r => r.json())
    .then(piada => {
        divPiada.innerText = piada.value;
        imgPiada.src = piada.icon_url;
        console.log(piada);
    })
}

piadaNorris();