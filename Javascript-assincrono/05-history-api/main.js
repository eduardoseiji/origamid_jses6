const links = document.querySelectorAll('a');
console.log(links);

function handleClick(e) {
    e.preventDefault();
    fetchPage(event.target.href);    
    window.history.pushState(null, null, event.target.href);
}

function replaceContent(newText) {
    const newHTML = document.createElement('div');
    newHTML.innerHTML = newText; 

    const oldContent = document.querySelector('.content');
    const newContent = newHTML.querySelector('.content');

    oldContent.innerHTML = newContent.innerHTML;    
    document.title = newHTML.querySelector('title').innerText;
}


async function fetchPage(url) {
    document.querySelector('.content').innerHTML = 'Carregando...';
    const pageResponse = await fetch(url);
    const pageText = await pageResponse.text();    
    replaceContent(pageText);    
}

window.addEventListener('popstate', () => {
    fetchPage(window.location.href);
})



links.forEach(link => {
    link.addEventListener('click', handleClick)
});