import outsideClick from './outsideclick.js';

export default function initDropdownMenu() {
  let liDropdown = document.querySelectorAll('[data-dropdown]');
  let eventosClique = ['touchstart', 'click'];
  
  liDropdown.forEach(menu => {
    eventosClique.forEach(userEvent => {
      menu.addEventListener(userEvent, handleClick);
    });
  });
  
  function handleClick(e) {
    e.preventDefault();
    this.classList.add('aberto');
    outsideClick(this, eventosClique, () => {
      this.classList.remove('aberto');
    });
  }
}