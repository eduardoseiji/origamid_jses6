// const promessa = new Promise((resolve, rejected) => {
//     let condicao = true;
//     if(condicao) {
//         setTimeout(() => {
//             resolve({nome: 'Eduardo', idade: 22});            
//         }, 1000);
//     } else {
//         rejected(Error('Um erro ocorreu na promise'));
//     }
// });

// const retorno = promessa.then((resolucao) =>{ 
//     console.log(resolucao.idade);
//     return 'Then 1'
// }).then((r) => {
//     return 'Then 2';
// }).catch(() => {
//     console.log('catchou');
// }).finally((item) => {
//     console.log('Acobou a paz');
// });

// console.log(retorno);

const login = new Promise((resolve, rejected) => {
    setTimeout(() => {
        resolve('Usuario logado');
    }, 1000);
});

const dados = new Promise((resolve, rejected) => {
    setTimeout(() => {
        resolve('Dados carregados');
    }, 1500);
});

const carregouTudo = Promise.race([login, dados]);
carregouTudo.then((resolucao) => {
    console.log(resolucao);
});