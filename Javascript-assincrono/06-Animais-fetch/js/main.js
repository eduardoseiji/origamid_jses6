import initScrollSuave      from './modules/scroll-suave.js';
import initAccordion        from './modules/accordion.js';
import initAnimaScroll      from './modules/scroll-animacao.js';
import initTabNav           from './modules/tab-nav.js';
import initModal            from './modules/modal.js';
import initTooltip          from './modules/tooltip.js';
import initDropdownMenu     from './modules/dropdown-menu.js';
import initMenuMobile       from './modules/menu-mobile.js';
import initFuncionamento    from './modules/funcionamento.js';
import initFetchAnimais     from './modules/fetch-animais.js';

initScrollSuave();
initAccordion();
initAnimaScroll();
initTabNav();
initModal();
initTooltip();
initDropdownMenu();
initMenuMobile();
initFuncionamento();
initFetchAnimais();