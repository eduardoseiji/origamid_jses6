
import initAnimaNumeros     from './anima-numeros.js';

export default function initFetchANimais() {

}

async function fetchAnimais(url) {
    const animaisReponse = await fetch(url);
    const animaisJSON = await animaisReponse.json();
    const gridNumeros = document.querySelector('.grid-numeros');
    animaisJSON.forEach(animal => {
        const divAnimal = createAnimal(animal);
        gridNumeros.appendChild(divAnimal);        
    }); 
    initAnimaNumeros();
}

function createAnimal(animal) {
    const div = document.createElement('div');
    div.classList.add('numero-animal');

    div.innerHTML = `<h3>${animal.specie}</h3><span data-numero>${animal.total}</span>`;
    return div;
}

fetchAnimais('./animais-api.json');