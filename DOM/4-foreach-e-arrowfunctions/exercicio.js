// Mostre no console cada parágrado do site
let paragrafos = document.querySelectorAll('p');
paragrafos.forEach((paragrafo) => console.log(paragrafo));
// Mostre o texto dos parágrafos no console
let arrayParagrafos = Array.from(paragrafos);
// console.log(arrayParagrafos);

arrayParagrafos.forEach((textoInterno) => {
    console.log(textoInterno.innerText);
});


// Como corrigir os erros abaixo:
const imgs = document.querySelectorAll('img');

imgs.forEach((item, index) => {
  console.log(item, index);
});

let i = 0;
imgs.forEach(() => {
  console.log(i++);
});

imgs.forEach(() => i++);