// Retorne no console todas as imagens do site
let todasImgs = document.querySelectorAll('img');
console.log(todasImgs);

// Retorne no console apenas as imagens que começaram com a palavra imagem
let todasImgPalavra = document.querySelectorAll('img[src^="img/imagem"]');
console.log(todasImgPalavra);

// Selecione todos os links internos (onde o href começa com #)
let linksInternos = document.querySelectorAll('a[href^="#"');
console.log(linksInternos);

// Selecione o primeiro h2 dentro de .animais-descricao
let animaisDescricaoSubitulo = document.querySelector('.animais-descricao h2');
console.log(animaisDescricaoSubitulo);

// Selecione o último p do site
let paragrafos = document.querySelectorAll('p');
let ultimoP = paragrafos[paragrafos.length - 1];
// let ultimoP = paragrafos[--paragrafos.length];
console.log(ultimoP);