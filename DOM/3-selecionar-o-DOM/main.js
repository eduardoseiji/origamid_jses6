//SELECIONA ELEMENTOS NO DOM E RETORNA O PRIMEIRO

const gridSection = document.getElementsByClassName('grid-section');
console.log(gridSection[0]);

const primeiraLi = document.querySelector('li');
console.log(primeiraLi);

const primeiraUl = document.querySelector('ul');
console.log(primeiraUl);

const linkInterno = document.querySelector('[href^="#"]');
console.log(linkInterno);


//SELECIONA ELEMENTOS NO DOM E RETORNA UM ARRAY/ARRAY-LIKE
const animaisImg = document.querySelectorAll('.animais img');

const gridSectionHTML = document.getElementsByClassName('grid-section'); //RETORNA HTMLELEMENT
const gridSectionNode = document.querySelectorAll('.grid-section'); //RETORNA NODELIST -> ESTATICA

primeiraUl.classList.add('grid-section'); //ADICIONA CLASSE NA PRIMEIRAUL

console.log(gridSectionHTML);
console.log(gridSectionNode);


gridSectionNode.forEach(function(item, index){
    console.log(item);
});

const arrayGrid = Array.from(gridSectionHTML); //TRANSFORMA ARRAY-LIKE EM ARRAY COM UM METODO DE ARRAY

arrayGrid.forEach(function(item){
    console.log(item);
});