// Duplique o menu e adicione ele em copy
{
    let copy = document.querySelector('.copy');
    let menu = document.querySelector('.menu');
    let novoMenu = menu.cloneNode(true);

    copy.appendChild(novoMenu);

}
// Selecione o primeiro DT da dl de Faq
{
    let primeiroDT = document.querySelector('.faq dl dt');
    console.log(primeiroDT);
}
// Selecione o DD referente ao primeiro DT
{
    let primeiroDT = document.querySelector('.faq dl dt');
    console.log(primeiroDT.nextElementSibling);
}
// Substitua o conteúdo html de .faq pelo de .animais
{
    let animais = document.querySelector('.animais');
    let faq = document.querySelector('.faq');

    faq.innerHTML = animais.innerHTML;
}