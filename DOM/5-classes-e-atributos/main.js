let menu = document.querySelector('.menu');


menu.classList.add('teste', 'linha');
menu.classList.remove('azul');
menu.classList.toggle('azul');
menu.classList.toggle('azul');
menu.classList.toggle('azul');

if(menu.classList.contains('azul')) {
    menu.classList.add('possui-azul');
} else {
    menu.classList.add('nao-possui-azul');
}
// console.log(menu.classList);


const animais = document.querySelector('.animais');

animais.attributes; // retorna todos os atributos (class, id, data etc)
animais.attributes[0]; // retorna o primeiro atributo

let img = document.querySelector('img');

let imgsrc = img.getAttribute('src');
console.log(imgsrc);

img.setAttribute('data-nome', 'raposa');

let possuiAlt = img.hasAttribute('alt');
console.log(possuiAlt);


const carro = {
    portas: 4,
    andar(km) {
        console.log(`Andou ${km} km`);
    }
}