// Adicione a classe ativo a todos os itens do menu
let itensMenu = document.querySelectorAll('.menu ul li');
itensMenu.forEach((item) => {
    item.classList.add('ativo');
});
// Remove a classe ativo de todos os itens do menu e mantenha apenas no primeiro
itensMenu.forEach((item) => {
    item.classList.remove('ativo');
});
itensMenu[0].classList.add('ativo');

// Verifique se as imagens possuem o atributo alt
{
    let imgs = document.querySelectorAll('img');

    imgs.forEach((img, index) => {
        if(!(img.hasAttribute('alt')) || 
             img.getAttribute('alt') === '' ||
             img.getAttribute('alt') === '#') {
            console.log(`A ${index}º imagem esta com alt errado`);
            console.log(img);
        }
    });
}


// Modifique o href do link externo no menu
let linkExterno = document.querySelector('a[href^="http"]');
console.log(linkExterno);
linkExterno.setAttribute('href', 'https://www.google.com.br');
console.log(linkExterno);