// Verifique a distância da primeira imagem
// em relação ao topo da página
let primeiraImagem = document.querySelector('img'),
    alturaPrimeiraImg = primeiraImagem.offsetTop;
console.log(alturaPrimeiraImg);

// Retorne a soma da largura de todas as imagens

function somaImagens() {
    let imagens = document.querySelectorAll('img');
    let soma = 0;
    imagens.forEach((imagem) => {
        soma += imagem.offsetWidth;
    });
    console.log(soma);
}

window.onload = function() {
    somaImagens();
}


// Verifique se os links da página possuem
// o mínimo recomendado para telas utilizadas
// com o dedo. (48px/48px de acordo com o google)
let links = document.querySelectorAll('a[href]');
console.log(links);

links.forEach((link) =>{
    if((link.offsetHeight <= 48) || (link.offsetWidth <= 48)){
        console.log('Ah nao meu');
    }
});

// Se o browser for menor que 720px,
// adicione a classe menu-mobile ao menu
const browserSmall = window.matchMedia('(max-width: 720px)').matches;
if(browserSmall) {
    const menu = document.querySelector('.menu');
    menu.classList.add('menu-mobile');
}
