// Quando o usuário clicar nos links internos do site,
// adicione a classe ativo ao item clicado e remova dos
// demais itens caso eles possuam a mesma. Previna
// o comportamento padrão desses links
{
    
    let linksInternos = document.querySelectorAll('a[href^="#"]');
    console.log(linksInternos);
    function handleLink(e) {
        e.preventDefault();

        linksInternos.forEach((link) => {
            link.classList.remove('ativo');
        });

        this.classList.add('ativo');
    }


    linksInternos.forEach((link) => {
        link.addEventListener('click', handleLink);
    });
}


// Selecione todos os elementos do site começando a partir do body,
// ao clique mostre exatamente quais elementos estão sendo clicados
// {
//     const todosElementos = document.querySelectorAll('body *');
//     console.log(todosElementos);

//     function callbackFunction(e) {
//         console.log(this);
//         this.remove();
//     }

//     todosElementos.forEach((elemento) => {
//         elemento.addEventListener('click', callbackFunction);
//     });
// }

// Utilizando o código anterior, ao invés de mostrar no console,
// remova o elemento que está sendo clicado, o método remove() remove um elemento


// Se o usuário clicar na tecla (t), aumente todo o texto do site. 
{
    function handleKeyboard(e) {
        if(e.key == 't') {
            console.log('t');
            document.documentElement.classList.toggle('textoMaior');
        }
    }
    addEventListener('keydown', handleKeyboard);
}