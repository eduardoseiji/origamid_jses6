// Retorne o url da página atual utilizando o objeto window
let urlPagina = window.location.href;
console.log(urlPagina);
// Seleciona o primeiro elemento da página que
// possua a classe ativo
let primeiroAtivo = document.querySelector('.ativo');
console.log(primeiroAtivo);

// Retorne a linguagem do navegador
let linguagem = window.navigator.language;
console.log(linguagem);
// Retorne a largura da janela 
console.log(innerWidth);