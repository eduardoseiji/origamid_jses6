import Countdown from './countdown.js';

const diasNatal = new Countdown('30 September 2019 19:00:00');

console.log(diasNatal.total);

const segundos  = document.querySelector('.segundos');
const minutos   = document.querySelector('.minutos');
const horas     = document.querySelector('.horas');
const dias     = document.querySelector('.dias');

setInterval(() => {

    

    segundos.innerText  = diasNatal.total.seconds;
    horas.innerText     = diasNatal.total.hours;
    minutos.innerText   = diasNatal.total.minutes;
    dias.innerText      = diasNatal.total.days;
}, 1000);