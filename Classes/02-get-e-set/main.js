const button = {
    get tamanho() {
        return 200;
    },
    set tamanho(numero) {
        this._numero = numero
    }    
}

const frutas = {
    lista: [],
    set nova(fruta) {
        this.lista.push(fruta);
    }
}

frutas.nova = 'Banana';
frutas.nova = 'Maça';

console.log(frutas);

class Button {
    constructor(text, background) {
        this.text = text;
        this.background = background;
    }
    get element() {
        const type = this._elementType || 'button';
        const buttonElement = document.createElement(type);
        buttonElement.innerText = this.text;
        buttonElement.style.background = this.background;
        return buttonElement;
    }
    set element(type) {
        this._elementType = type;
    }
}

const blueButton = new Button('Comprar', 'green').element;
document.documentElement.appendChild(blueButton);