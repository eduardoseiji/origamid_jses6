// function Button(text, background) {
//     this.text = text;
//     this.background = background;
// }

// Button.prototype.element = function() {
//     const buttonElement = document.createElement('button');
//     buttonElement.innerText         = this.text;
//     buttonElement.style.background  = this.background;
//     return buttonElement;
// }
class Button {
    constructor(text, background, color) {
        this.text = text;
        this.background = background;
        this.color = color;
    }
    element() {
        const buttonElement = document.createElement('button');
        buttonElement.innerText         = this.text;
        buttonElement.style.background  = this.background;
        return buttonElement;
    }
    appendTo(target) {
        const targetElement = document.querySelector(target);
        console.log(this);
        
        targetElement.appendChild(this.element());
    }
    static blueButton(text) {
        return new Button(text, '#00F', '#FFF');
    }
}

const blueButton = Button.blueButton('Oi Josuke');
console.log(blueButton);

// class Button {
//     constructor(options) {
//         this.options = options;
//     }
//     static createButton(text, background, color) {
//         const buttonElement = document.createElement('button');
//         buttonElement.innerText = text;
//         buttonElement.style.background = background;
//         buttonElement.style.color = color;
//         return buttonElement;
//     }
//     st
// }

// const blueButton = new Button({
//     backgroundColor: 'blue',
//     textColor: 'white',
//     fontWeight: 700,
//     text: 'Comprar'
// });


// const redButton = Button.createButton('Deletar', 'red', 'white');

// console.log(redButton);

// document.documentElement.appendChild(redButton);