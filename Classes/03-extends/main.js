class Veiculo {
    constructor(rodas, combustivel) {
        this.rodas = rodas;
        this.combustivel = combustivel;
    }
    acelerar() {
        console.log('Acelerou veiculo');
    }
}

class Moto extends Veiculo {
    constructor(rodas, combustivel, capacete) {
        super(rodas, combustivel);
        this.capacete = capacete;
    }
    empinar() {
        console.log(`empinou com ${this.rodas} rodas`);        
    }
    acelerar() {
        super.acelerar();
        console.log('Aoooo acelerou');
    }
}

const honda = new Moto(4, true, true);
const civic = new Veiculo(4, 'ae');
console.log(honda);

honda.acelerar();